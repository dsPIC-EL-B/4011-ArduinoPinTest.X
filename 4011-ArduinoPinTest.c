/*! \file  4011-ArduinoPinTest.c
 *
 *  \brief
 *  This is a fairly dumb test that uses the Arduino library to
 *  exercise all the pins brought out to a shield.  Pins are flashed
 *  in sequence, then all pins at once three times.
 *
 *  Note that pins A4 and A5 are actually I2C pins, which are shared with
 *  SPI pins, so they flash when pins 11 and 12 flash, and not when
 *  A4 and A5 should.
 *
 *
 *  \author jjmcd
 *  \date March 26, 2013, 11:11 AM
 *
 * Software License Agreement
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 */

#include <xc.h>
#include "../include/Arduino.h"
#include "../include/lcd.h"

// Configuration fuses
//! Use crystal oscillator, 16X PLL for 29.5 MIPS clock
_FOSC (XT_PLL16);
//! Watchdog timer off
_FWDT (WDT_OFF);
//! 16ms Power on timer, 2.7V brownout detect, enable MCLR
_FBORPOR (PWRT_16 & BORV27 & MCLR_EN);
//! No code protection
_FGS( GWRP_OFF & CODE_PROT_OFF );

#define DELAY_TIME 50

/*! blinkPin - Flash a pin */
/*! Sets the selected pin high, waits a short time, sets it low,
 *  and then waits for half the time it was on.
 */
void blinkPin( int nPin )
{
    digitalWrite( nPin, HIGH );
    delay( 2*DELAY_TIME );
    digitalWrite( nPin, LOW );
    delay( DELAY_TIME );
}

/*! main - Test Arduino pin assignments */
/*! Flashes all the pins brought out to the Arduino shield.
 */
int main(int argc, char *argv[])
{
    int i,j;

    LCDinit();
    LCDclear();
    LCDputs("Arduino Pin Test");
    for ( i=0; i<=A5; i++ )
        pinMode( i, OUTPUT );

    while ( 1 )
    {
        LCDline2();
        LCDputs("   Cycle pins    ");
        digitalWrite(4,LOW);
        for ( i=0; i<=A5; i++ )
            blinkPin( i );

        LCDline2();
        LCDputs("                ");
        digitalWrite(4,LOW);
        delay( 10*DELAY_TIME );

        LCDline2();
        LCDputs("   Blink all    ");
        digitalWrite(4,LOW);
        for ( j=0; j<5; j++ )
        {
            for ( i=0; i<=A5; i++ )
                digitalWrite( i, HIGH );
            delay( 2*DELAY_TIME );
            for ( i=0; i<=A5; i++ )
                digitalWrite( i, LOW );
            delay( 2*DELAY_TIME );
        }

        LCDline2();
        LCDputs("                ");
        digitalWrite(4,LOW);
        delay( 10*DELAY_TIME );

    }

    return 0;
}
